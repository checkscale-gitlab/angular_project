import { Component, OnInit, Input } from '@angular/core';
import { Image } from '../../core/models/image.model';
import { ImageService } from '../../core/services/image.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, tap } from 'rxjs';

@Component({
  selector: 'app-single-image',
  templateUrl: './single-image.component.html',
  styleUrls: ['./single-image.component.scss']
})
export class SingleImageComponent implements OnInit {

  image!: Image;
  image$!: Observable<Image>

  constructor(private imageService: ImageService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const pathId = +this.route.snapshot.params['id'];
    this.image$ = this.imageService.getImage(pathId)
  }

  onClick(id:number) {
    this.imageService.onClick(id).pipe(
      tap(() => {
        this.image$ = this.imageService.getImage(id);
      })
    ).subscribe();
  }


}

export class Image {

  id!: number;
  title!: string;
  imageUrl!: string;
  createdDate!: Date;
  clicks!: number;
  location?: string;
}
